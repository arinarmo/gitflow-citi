---
title: "Ejercicio_Modulo_R"
author: " AM | KG | GG"
date: "6 de octubre de 2018"
output: html_document
---

#Nombres: Ana Mereles | Karen Gaona | Giovanni Guerrero

### **Objetivo:** Utilizar la información por unidad económica, codigo postal, y entender cuáles son las caractersíticas en cuanto a población, ingreso, ocupación, empresas, etc con el fin de poder usar esta información como una información inicial o complemento a la información de un cliente individual

### **Uso:** Puede ser útil para que el banco establezca clusters de zonas con características parecidas con el fin de hacer campañas publicitarias adecuadas y ofrecer los mejores productos en las sucursales.
<br>

#### **1. Importando librerías**
```{r}
library(readxl)
library(knitr)
library(kableExtra)
library(tidyr)
library(dplyr)
library(ggplot2)
library(RColorBrewer)
```

<br>

#### **1. Importando Dataset**
```{r}
#setwd("~/Diplomado citi/Caso practico")
#df1 <- read_excel("Denue012016_Agentessurbanos demograficos.xlsx")
Schema <- read.csv("Informacion.csv")
df1<-read.csv("Denue012016_Agebs.csv")
```
 
 <br>
 
#### **4. Exploración de datos**
 + ¿Cuáles son sus primeros registros?
```{r}
  Primeros=head(df1)
  Primeros %>% 
  kable() %>%
    kable_styling() %>% scroll_box(width = "100%", height = "400px")
```

 <br>
 
 + Observaciones del Dataset

```{r}
paste0("El data set tiene ",nrow(df1)," registros y ",ncol(df1)," atributos")

```

<br>

 + Unidad Observacional: ID_AGEB : Claves INEGI concatenadas de: Estado, Municipio, Localidad y Ageb

<br>
 
 + Tipo de Datos : Observamos que las variables con tipo de dato caracter, en realidad deberían ser categoricas
```{r}
cols <- c("ZMINEGI","Estado","ID_AGEB","Municipio","NSE_2016","CodPostal","AGEB","Ageb2","Cvemun","Cveloc","CVEINEGI","Cveedo")
ids=1:ncol(df1)
nombres=names(df1)
ids=ids[nombres%in% cols]

sapply(df1, class)%>%
kable() %>%
  kable_styling()%>%row_spec(ids, bold = T, color = "white", background = "#003B70") %>% scroll_box(width = "100%", height = "500px")

```

##### Tenemos algunas variables identificadasen azul que no estan como factor
```{r}

cols <- c("ZMINEGI","Estado","ID_AGEB","Municipio","NSE_2016","CodPostal","AGEB","Ageb2","Cvemun","Cveloc","CVEINEGI","Cveedo")

df1[cols]<- lapply(df1[cols], factor)

sapply(df1[,cols], class)%>%
kable() %>%
  kable_styling()%>% scroll_box(width = "100%", height = "500px")

```


<br>

#### **6. Resumen de variables** 
```{r}

Unique_values<-function(Data,n=3){
   valores=NULL
  unicos=sapply((Data), unique)
  for(i in unicos){
    m=ifelse(length(i)<n,length(i),n)
    valores=c(valores,paste0(i[1:m],collapse = ", "))
  }
  return(data.frame(Variable=names(Data),Tipo=sapply(Data, class),Posibles_valores=valores))
}
unicos=Unique_values(df1)
ver=merge(x=Schema,y=unicos)

ver%>%
kable() %>%
  kable_styling()%>%scroll_box(width = "100%", height = "500px")

```


<br>

#### **7. Estadísticos y Gráficos** 

<br>

* **Distribución de Población por Estado** 
```{r}
by_state_pop <- df1 %>% 
                group_by(Estado) %>%
               summarise(sum_Pob_Total=sum(as.numeric(Pob_total)))
Total_poblacion=sum(by_state_pop$sum_Pob_Total)
by_state_pop <- cbind(by_state_pop,d_poblacion=by_state_pop$sum_Pob_Total/Total_poblacion)

#  Definimos el numero de colores necesarios 
colourCount = 32;
# Creamos funcion para obtener los colores que requerimos de una paleta especifica
library(scales)
getPalette = colorRampPalette(brewer.pal(9, "Set1"))
ggplot(by_state_pop, aes(x=1:32, y=d_poblacion,fill=Estado)) + geom_bar(stat="identity",position="dodge")+scale_fill_manual(values = getPalette(colourCount))+ ggtitle("Distribución de la poblacion por estado") + xlab("Estado") + theme(plot.title = element_text(hjust = 0.5),axis.text.x = element_blank()) + ylab("Población")+ scale_y_continuous(labels = percent) 


```

<br>

+ El estado con mayor población es el Estado de México, seguido por la Ciudad de México, con una diferencia de casi 600pb. Entre ambos estados, concentran casi el 25% de toda la población de la República Mexicana.  

+ Los estados con una concentración menor al 1% de la población son Colima, Baja California Sur, Campeche y Nayarit.

<br>

* **Distribución de Población por Estado y genero** 
```{r}
               
by_state_pop <- df1 %>% gather(key=sexo,value=Poblacion_Genero,Pob_Hombres:Pob_Mujeres)%>% group_by(Estado,sexo) %>%
               summarise(sum_Pob_Total=sum(as.numeric(Poblacion_Genero)))

total <- by_state_pop %>% group_by(Estado) %>%
               summarise(Total=sum(as.numeric(sum_Pob_Total)))

consolidado<-merge(by_state_pop,total,all.x = T)


consolidado<-consolidado%>%
              mutate(distribution=sum_Pob_Total/Total)

ggplot(consolidado, aes(x=Estado , y=distribution,fill=sexo)) + geom_bar(stat="identity")+ ggtitle("Distribución de la poblacion por estado") + xlab("Estado") + theme(plot.title = element_text(hjust = 0.5),axis.text.x = element_text(angle = 60, hjust = 1)) + ylab("Población")+ scale_y_continuous(labels = percent) 


```
<br>

 + La distribución por género se muestra bastante estable por estado. 


<br> 
 
#### Haciendo zoom en el estado con mayor población
 
 
 <br>
```{r}
by_state <- df1 %>%
            filter(Estado=="México")%>%
            group_by(Municipio) %>%
            summarise(sum_Pob_total=sum(as.numeric(Pob_total)))


by_municipio <- by_state %>%
                filter(sum_Pob_total==max(sum_Pob_total))

paste0("Dentro del Estado de México el municipio con mayor población es ", by_municipio$Municipio, " con " , by_municipio$sum_Pob_total, " habitantes.")

by_municipio <- by_state %>%
                filter(sum_Pob_total==min(sum_Pob_total))

paste0("Dentro del Estado de México el municipio con menor población es ", by_municipio$Municipio, " con " , by_municipio$sum_Pob_total, " habitantes.")


```
 <br>
 
 + Distribución de Hombres y Mujeres en Ecatepec de Morelos
```{r}
by_Ecatepec <- df1 %>%
            filter(Municipio=="Ecatepec de Morelos")

df5 <- by_Ecatepec %>%
        gather(key=sexo,value=sexo_poblacional,Pob_Hombres:Pob_Mujeres)

by_gender <- df5 %>% 
                group_by(sexo) %>%
                summarise(sum_Pob_Total=sum(as.numeric(sexo_poblacional)))

by_gender <- cbind(by_gender,s_total=sum(by_gender$sum_Pob_Total))
by_gender <- cbind(by_gender,d_poblacion=100*by_gender$sum_Pob_Total/by_gender$s_total)

ggplot(by_gender, aes(x=1:2, y=d_poblacion,fill=sexo)) + geom_bar(stat="identity")+ ggtitle("% de Hombres y Mujeres en Ecatepec de Morelos") + xlab("Sexo") + theme(plot.title = element_text(hjust = 0.5)) + ylab("%Población") +scale_fill_brewer(palette="Set1")


```


<br>

 + Se puede observar que hay una ligera diferencia en la distribución por género en donde la proporción de número de mujeres por cada hombre es de 1.05.

<br>

* **Distribución de Población por Edad en México** 
```{r}

df2 <- df1 %>%
        gather(key=Edad,value=edad_poblacional,Pob_0a12:Pob_66ymas)

by_age <- df2 %>% 
                group_by(Edad) %>%
                summarise(sum_Pob_Total=sum(as.numeric(edad_poblacional)))

by_age <- cbind(by_age,s_total=sum(by_age$sum_Pob_Total))
by_age <- cbind(by_age,d_poblacion=100*by_age$sum_Pob_Total/by_age$s_total)
colourCount=10
getPalette = colorRampPalette(brewer.pal(12, "Paired"))
ggplot(by_age, aes(x=1:10, y=d_poblacion,fill=Edad)) + geom_bar(stat="identity")+ ggtitle("%Población Total Por Edad") + xlab("Edad") + theme(plot.title = element_text(hjust = 0.5)) + ylab("Población")+scale_fill_manual(values = getPalette(colourCount))


```

 + Una cuarta parte de la población mexicana está concentrada en edades menores a 12 años, lo cual representa una oportunidad en el mercado para la atracción de clientes nuevos y una mejor cultura financiera en el país.En México existen pocos bancos con cuentas de ahorro para menores de edad, sin embargo ellos formarán la base de clientes a los cuales otorgar crédito en el futuro. 

<br>

* **Distribución de Empresas por Estado** 
```{r}

df3 <- df1 %>%
        gather(key=Tipo_Empresas,value=Empresas, Administrativos:Servicios)

by_empresa <- df3 %>% 
                group_by(Estado) %>%
                summarise(sum_Empresas=sum(as.numeric(Empresas)))

by_empresa <- cbind(by_empresa,s_total=sum(by_empresa$sum_Empresas))
by_empresa <- cbind(by_empresa,d_poblacion=100*by_empresa$sum_Empresas/by_empresa$s_total)
getPalette = colorRampPalette(brewer.pal(9, "Paired"))
colourCount=32
ggplot(by_empresa, aes(x=1:32, y=d_poblacion,fill=Estado)) +  geom_bar(stat="identity")+ ggtitle("%Empresas por Estado") + xlab("Estado") + theme(plot.title = element_text(hjust = 0.5),axis.text.x = element_blank()) + ylab("%Empresas") +scale_fill_manual(values = getPalette(colourCount))


```

 + En tan sólo siete estados podemos encontrar aproximadamente el 50% del total de las empresas del páis, con lo cuál se está expandiendo la actividad económica fuera de la capital y la zona metropolitana y se está migrando a diferentes estados generando nuevas oportunidades de empleo. 
 
 + Este tipo de información permite a las instituciones financeras generar indicadores de crecimiento ecónomico basados en geolocalización para ser incluidos en las políticas de otorgamiento de crédito y no sólo considerar datos del cliente. 


<br>

* **Distribución de Tipo de Empresas por Nivel Socioeconómico en los estados con mayor actividad empresarial** 

```{r}
mas_emp<- c("Distrito Federal","México","Jalisco","Puebla","Veracruz de Ignacio de la Llave","Guanajuato")

by_mas_emp <- df1 %>%
            filter(Estado %in% mas_emp)

df4 <- by_mas_emp %>%
        gather(key=Tipo_Empresas,value=Empresas, Administrativos:Servicios)

by_empresa_econ <- df4 %>% 
                group_by(NSE_2016,Tipo_Empresas) %>%
                summarise(sum_Empresas=sum(as.numeric(Empresas)))

by_empresa <- by_empresa_econ %>% 
                group_by(Tipo_Empresas) %>%
                summarise(sum_Total_Empresas=sum(as.numeric(sum_Empresas)))

by_empresa_econ2 <- merge(by_empresa_econ,by_empresa, by="Tipo_Empresas",all.x = TRUE)
by_empresa_econ2 <- cbind(by_empresa_econ2,d_nse=by_empresa_econ2$sum_Empresas/by_empresa_econ2$sum_Total_Empresas)


ggplot(by_empresa_econ2, aes(x=Tipo_Empresas, y=d_nse,fill=NSE_2016)) +  geom_bar(stat="identity")+ ggtitle("Distribucion de empresas por tipo y NSE") + xlab("Tipo Empresas") + theme(plot.title = element_text(hjust = 0.5),axis.text.x = element_text(angle = 15, hjust = 1)) + ylab("Empresas")  +scale_fill_brewer(palette="Paired")



```

 + En esta gráfica se analizaron sólo los estados con mayor concentración de empresas dentro de los cuales están Distrito Federal, Estado de México, Jalisco, Puebla, Michoacán, Guanajuato y Veracruz. Se puede observar que en los niveles socioeconómicos bajos, es decir con un nivel de vida austero, hay un mayor porcentaje de primarias y empresas de manufactura, mientras que en los niveles socioecónomicos medio-alto encontramos restaurantes, hoteles, empresas de servicios y administrativas.

<br>


#####A partir de los datos analizados podemos concluir que se necesita información más detallada sobre la ubicación de nuestros clientes para poder robustecer los servicios que brinda el banco actualmente. Se pueden crear politicas, productos o distintos tipos de beneficios dependiendo del lugar en donde vive, realizar análisis con geolocalización para conocer el entorno, actividad transaccional y crecimiento económico de la zona en los próximos años, todo con el objetivo de brindar una atención personalizada a cada cliente. 

<br>


#### **BONUS** 

<br>

#### **1. Importando Dataset para cruzar con información actual**
```{r}
Adicional <- read_excel("income_basejam.xlsx")

nueva= df1%>%gather(key=edad,value = personas,Pob_18a20:Pob_66ymas )%>%select(NSE_2016,edad,personas,CodPostal)
nueva$CodPostal=as.factor(nueva$CodPostal)
nueva2=nueva%>%group_by(CodPostal,edad)%>%summarise(maximo=max(personas,na.rm = T))
nueva3=left_join(x=nueva2, y=nueva,by=c("CodPostal"="CodPostal","edad"="edad","maximo"="personas"))
Adicional$codigo_postal=as.numeric(Adicional$codigo_postal)
Adicional$Segmento_Edad=ifelse(Adicional$EDAD<=20,"Pob_18a20",
                          ifelse(Adicional$EDAD<=25,"Pob_21a25",
                                 ifelse(Adicional$EDAD<=35,"Pob_26a35",
                                          ifelse(Adicional$EDAD<=45,"Pob_36a45",
                                                   ifelse(Adicional$EDAD<=55,"Pob_46a55",
                                                            ifelse(Adicional$EDAD<=65, "Pob_56a65","Pob_66ymas"))))))
Adicional$codigo_postal=as.factor(Adicional$codigo_postal)
bonus=left_join(x=Adicional,y=nueva3,by=c("codigo_postal"="CodPostal","Segmento_Edad"="edad"))
bonus$contador=1;
bonus2= bonus%>%group_by(NSE_2016,Segmento_Edad)%>%summarise(n=sum(contador))
bonus3= bonus%>%group_by(NSE_2016)%>%summarise(total=sum(contador))
bonus4=left_join(x=bonus2,y=bonus3,by=c("NSE_2016"="NSE_2016"))
bonus4$ratio=bonus4$n/bonus4$total

```

Antes de realizar el cruce fue necesario realizar una serie de transormaciones para poder tener la categoria a la que pertenece cada cliente.
+ Observaciones del Nuevo Dataset

```{r}
paste0("El nuevo data set tiene ",nrow(bonus)," registros y ",ncol(bonus)," atributos")

```
<br>
Distribucion de solicitudes NSE y rangos de edad de los clientes 
```{r}
  ggplot(bonus4, aes(x=NSE_2016, y=ratio,fill=Segmento_Edad)) +  geom_bar(stat="identity")+ ggtitle("Distribucion de solicitudes NSE y rangos de edad") + xlab("NSE") + theme(plot.title = element_text(hjust = 0.5),axis.text.x = element_text(angle = 15, hjust = 1)) + ylab("Distribución")  +scale_fill_brewer(palette="Paired")
```
<br>
Podemos observar en general las distribuciones se mantienen, salvo en el segmento A/B+ en donde existe una mayor concentración en edades avanzadas.
