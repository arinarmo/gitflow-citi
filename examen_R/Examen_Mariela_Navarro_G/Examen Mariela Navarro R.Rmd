---
title: "Examen (Clase 4 - 5)"
author: "Mariela Navarro"
date: "07 de Octubre 2018"
output:
  pdf_document: default
  html_document: default
---

```{r knitr::opts_chunk$set, echo=FALSE, warning=FALSE, message=FALSE}
# Corre librerías
library(readxl)
library(dplyr)
library(ggplot2)
library(devtools)
#library(mxmaps)
library(tidyr)
```

***

> El Dataset del Directorio Estadístico Nacional de Unidades Económicas que se analizará incluye información como edad, sexo, ingreso de la población, número de empresas por número de trabajadores y por sector económico a nivel Estado, Municipio, Localidad y Ageb. <br> <br>
Esta información es util para conocer, de manera general, las condiciones económicas y sociales que se viven en las distintas localidades en México; lo anterior podría ser utilizado para realizar con mayor efectividad investigaciones que ayuden a detonar nuevas estrategias de crecimiento y desarrollo como: <br> <br>
&nbsp; - Oferta correcta de productos a los clientes. <br>
&nbsp; - Entendimiento del rol de las Sucursales por zona. <br>
&nbsp; - Asignación correcta del número de ejecutivos. <br>
&nbsp; - Identificación de zonas potenciales a crecimiento. <br>

***


### 1. &nbsp;Importa Dataset

```{r}
# Asigna directorio
setwd("/Users/mariela/Documents/Diplomado/Bases de Datos")

# Importa archivo en formato Excel
datos <- read_excel("Denue012016_Agentessurbanos demograficos.xlsx")

# Convierte en Dataframe
datos <- as.data.frame(datos)
```
***

### 2. &nbsp;Preparación de la base

***

#### 2.1 &nbsp;Ajuste a formato de la base 

Asigna encabezados correctamente:

```{r}
# Elimina los dos primeros renglones que contienen: un renglón vacío (NA) y un renglón con un "Segundo Header"
datos <- datos[3:49120,]

# Crea vector correcto de encabezados (con minúsculas y sin espacios)
headers_datos <- c("anio_info","id_ageb","zminegi","estado","municipio","nse_2016","codpostal","ageb","ageb2","pob_total","pob_0a12","pob_13a15","pob_16a17","pob_18a20","pob_21a25","pob_26a35","pob_36a45","pob_46a55","pob_56a65","pob_66ymas","pob_18ymas","pob_hombres","pob_mujeres","hog_tot","hog0_2","hog2_4","hog4_7","hog7_11","hog11_16","hog16_21","hog21_41","hog21ymas","hog41_61","hog61_101","hog101_mas","tot_empresas","administrativos","comercio_mayor","comercio_menor","manufactura","primarias","restaurantes_hoteles","servicios","perocup0a5","perocup6a10","perocup11a30","perocup31a50","perocup51a100","perocup101a250","perocup251ymas","cveinegi","cveedo","cvenum","cveloc")

# Asigna el nuevo vector de encabezados a la base
names(datos) <- headers_datos
```

***

#### 2.2 &nbsp;Exploración de Datos

* La Unidad Observacional considera la combinación de Estado, Municipio, Localidad y Área Geoestadística Básica

* El Dataset es un "Data Frame" que contiene 49,118 registros y 54 variables de tipo caracter; en el siguiente cuadro se pueden observar los primeros valores que toman las distintas variables que conforman el Data Frame, además se observa que hay algunas variables de tipo categórico y numérico que no están siendo leídas correctamente:


```{r  echo=FALSE}
str(datos)
```

***

#### 2.3 &nbsp;Ajuste de tipo de Datos

Se realiza el ajuste a las variables que tienen mal asignado el tipo de dato:

```{r}
# Selecciona variables que van en formato numérico y categórico
v_formato_num <- c("pob_total","pob_0a12","pob_13a15","pob_16a17","pob_18a20","pob_21a25","pob_26a35","pob_36a45","pob_46a55","pob_56a65","pob_66ymas","pob_18ymas","pob_hombres","pob_mujeres","hog_tot","hog0_2","hog2_4","hog4_7","hog7_11","hog11_16","hog16_21","hog21_41","hog21ymas","hog41_61","hog61_101","hog101_mas","tot_empresas","administrativos","comercio_mayor","comercio_menor","manufactura","primarias","restaurantes_hoteles","servicios","perocup0a5","perocup6a10","perocup11a30","perocup31a50","perocup51a100","perocup101a250","perocup251ymas")
v_formato_cat <- c("zminegi","estado","municipio","nse_2016","codpostal","ageb","ageb2","cveedo","cvenum","cveloc")

# Cambia formato de las variables
datos[v_formato_num] <- lapply(datos[v_formato_num], as.numeric)
datos[v_formato_cat] <- lapply(datos[v_formato_cat], factor)

```
Confirma el nuevo tipo de dato sobre las variables:

```{r  echo=FALSE}
sapply(datos, class)
```

***

#### 2.4 &nbsp;Descripción de Variables

![](/Users/mariela/Documents/Diplomado/Bases de Datos/Descripción Variables Denue.png)

***

<br>

### 3. &nbsp;Análisis de los Datos

***

#### 3.1 &nbsp;Agrupación de variables

Se generan nuevas variables utilizando las actuales para tener menos categorías y poder entender de mejor manera la información:
<br>

##### Variable Edad:

* **Niños:** entre 0 y 15 años &nbsp; &nbsp; &nbsp; _(pob_ninos)_
* **Jóvenes:** entre 16 y 25 años &nbsp; &nbsp; &nbsp; _(pob_jovenes)_
* **Adultos jóvenes:** entre 26 y 45 años &nbsp; &nbsp; &nbsp; _(pob_adu_jov)_
* **Adultos:**  entre 46 y 65 años &nbsp; &nbsp; &nbsp; _(pob_adultos)_
* **Adultos mayores:**  más de 66 años &nbsp; &nbsp; &nbsp; _(pob_ad_mayores)_

```{r warning=FALSE}
# Genera nuevos rangos para la variable Edad:  
datos <- datos %>% mutate(pob_ninos=pob_0a12+pob_13a15, pob_jovenes=pob_16a17+pob_18a20+pob_21a25,      pob_adu_jov=pob_26a35+pob_36a45, pob_adultos=pob_46a55+pob_56a65)
# Ajusta nombres de variables
colnames(datos)[colnames(datos)=="pob_66ymas"] <- c("pob_ad_mayores")
```
<br>

##### Variable Ingreso por trabajo en el Hogar:

* **Ingresos Marginales:** entre 0 y 2 salarios mínimos &nbsp; &nbsp; &nbsp; _(ing_marginal)_
* **Ingresos Bajos:** entre 2 y 4 salarios mínimos &nbsp; &nbsp; &nbsp; _(ing_bajo)_
* **Ingreso Medio:** entre 4 y 11 salarios mínimos &nbsp; &nbsp; &nbsp; _(ing_medio)_
* **Ingreso Medio-Alto:** entre 11 y 21 salarios mínimos &nbsp; &nbsp; &nbsp; _(ing_med_alto)_
* **Ingreso Alto:**  más de 21 salarios mínimos &nbsp; &nbsp; &nbsp; _(ing_alto)_

```{r warning=FALSE}
# Genera nuevos rangos para la variable Ingreso al Hogar:  
datos <- datos %>% mutate(ing_medio=hog4_7+hog7_11, ing_med_alto=hog11_16+hog16_21)
# Ajusta nombres de variables
colnames(datos)[colnames(datos)=="hog0_2"] <- c("ing_marginal")
colnames(datos)[colnames(datos)=="hog2_4"] <- c("ing_bajo")
colnames(datos)[colnames(datos)=="hog21ymas"] <- c("ing_alto")
```
<br>

##### Variable tamaño de las Empresas:

* **Micro Empresa:** entre 1 y 10 empleados &nbsp; &nbsp; &nbsp; _(emp_micro)_
* **Pequeña Empresa:** entre 11 y 100 empleados &nbsp; &nbsp; &nbsp; _(emp_pequena)_
* **Mediana Empresa:** entre 100 y 250 empleados &nbsp; &nbsp; &nbsp; _(emp_mediana)_
* **Grande Empresa:** más de 250 empleados &nbsp; &nbsp; &nbsp; _(emp_grande)_

```{r warning=FALSE}
# Genera nuevos rangos para la variable Tamaño de la Empresa:  
datos <- datos %>% mutate(emp_micro=perocup0a5+perocup6a10, emp_pequena=perocup11a30+perocup31a50+perocup51a100)
# Ajusta nombres de variables
colnames(datos)[colnames(datos)=="perocup101a250"] <- c("emp_mediana")
colnames(datos)[colnames(datos)=="perocup251ymas"] <- c("emp_grande")
```

***

#### 3.2 &nbsp;Medidas Estadísticas Básicas

Para poder hacer un análisis general, la información será analizada a nivel Estado que es el nivel más alto que tiene el Dataset;  es importante mencionar que todo lo que será mostrado a continuación puede ser analizado de igual manera sobre los otros niveles de la base (Municipio, Localidad y Área Geoestadística Básica).

Con ayuda de la función dyplr se realiza el ajuste de variables para poder hacer el análisis:

```{r warning=FALSE}
# VARIABLE SEXO
# Se agrupan las variables a nivel Estado
dist_poblacion <- datos %>% select(estado,pob_hombres,pob_mujeres) %>% group_by(estado) %>% summarise(B_Hombres=sum(pob_hombres),A_Mujeres=sum(pob_mujeres)) %>% mutate(pctg_hombres = B_Hombres/(B_Hombres+A_Mujeres)) %>% arrange(desc(B_Hombres+A_Mujeres)) 
# Se separa la base a nivel Sexo para poder graficar
dist_poblacion2 <- dist_poblacion %>% gather(key = Sexo, value = Tot_pob_total, B_Hombres:A_Mujeres)

# VARIABLE EDAD
# Se agrupan las variables a nivel Estado 
dist_edad <- datos %>% select(estado,pob_ninos,pob_jovenes,pob_adu_jov,pob_adultos,pob_ad_mayores) %>% group_by(estado) %>% summarise(A_Ninos=sum(pob_ninos),B_Jovenes=sum(pob_jovenes),C_Adultos_Jovenes=sum(pob_adu_jov),D_Adultos=sum(pob_adultos),E_Adultos_Mayores=sum(pob_ad_mayores))
# Se separa la base a nivel Edad para poder graficar
dist_edad <- dist_edad %>% gather(key = Edad, value = Tot_pob_total, A_Ninos:B_Jovenes:C_Adultos_Jovenes:D_Adultos:E_Adultos_Mayores)

#VARIABLE INGRESO
# Se agrupan las variables a nivel Estado
dist_ingreso <- datos %>% select(cveedo,estado,pob_total,ing_marginal,ing_bajo,ing_medio,ing_med_alto,ing_alto,pob_hombres,pob_mujeres) %>%  group_by(cveedo,estado) %>% summarise(Tot_ing_marginal=sum(ing_marginal), Tot_ing_bajo=sum(ing_bajo), Tot_ing_medio=sum(ing_medio), Tot_ing_med_alto=sum(ing_med_alto), Tot_ing_alto=sum(ing_alto)) %>% mutate(E_Ing_Marginal=Tot_ing_marginal/(Tot_ing_marginal+Tot_ing_bajo+Tot_ing_medio+Tot_ing_med_alto+Tot_ing_alto), D_Ing_Bajo=Tot_ing_bajo/(Tot_ing_marginal+Tot_ing_bajo+Tot_ing_medio+Tot_ing_med_alto+Tot_ing_alto), C_Ing_Medio=Tot_ing_medio/(Tot_ing_marginal+Tot_ing_bajo+Tot_ing_medio+Tot_ing_med_alto+Tot_ing_alto), B_Ing_Med_Alto=Tot_ing_med_alto/(Tot_ing_marginal+Tot_ing_bajo+Tot_ing_medio+Tot_ing_med_alto+Tot_ing_alto), A_Ing_Alto=Tot_ing_alto/(Tot_ing_marginal+Tot_ing_bajo+Tot_ing_medio+Tot_ing_med_alto+Tot_ing_alto))  %>% arrange(desc(E_Ing_Marginal))
# Se separa la base a nivel Tipo Ingreso para poder graficar
dist_ingreso2 <- dist_ingreso %>% gather(key = Tipo_Ingreso, value = Pob_share, E_Ing_Marginal:D_Ing_Bajo:C_Ing_Medio:B_Ing_Med_Alto:A_Ing_Alto)

#NIVEL DE BIENESTAR ECONÓMICO
# Se agrupan las variables a nivel Estado, Nivel de Bienestar
dist_bienestar_n2 <- datos %>% select(estado,nse_2016,pob_total) %>% group_by(estado,nse_2016) %>% summarise(Tot_poblacion_NB=sum(pob_total))
# Se agrupan las variables a nivel Estado
dist_bienestar_n1 <- datos %>% select(estado,pob_total) %>% group_by(estado) %>% summarise(Tot_poblacion_E=sum(pob_total))
# Pega el total por Estado a base con información de Nivel de Bienestar
dist_bienestar <- merge(dist_bienestar_n2,dist_bienestar_n1,by="estado")
# Calcula %
dist_bienestar$"pctg_edo"=(dist_bienestar$Tot_poblacion_NB/dist_bienestar$Tot_poblacion_E)*100

#VARIABLE NUMERO DE EMPRESAS
# Se agrupan las variables a nivel Estado
dist_empresas <- datos %>%select(estado,emp_micro,emp_pequena,emp_mediana,emp_grande) %>% group_by(estado) %>% summarise(A_Micro=sum(emp_micro),B_pequena=sum(emp_pequena),C_mediana=sum(emp_mediana),D_grande=sum(emp_grande)) %>% arrange(desc(A_Micro+B_pequena+C_mediana+D_grande)) 
# Se separa la base a nivel Tipo Empresa para poder graficar
dist_empresas2 <- dist_empresas %>% gather(key = Tamano_Empresa, value = Tot_empresas, A_Micro:B_pequena:C_mediana:D_grande)

#TIPO DE EMPRESAS
# Se agrupan las variables a nivel Estado 
dist_tipo_emp <- datos %>% select(estado,administrativos,comercio_mayor,comercio_menor,manufactura,primarias,restaurantes_hoteles,servicios) %>% group_by(estado) %>% summarise(A_Administrativos=sum(administrativos),B_Comercio_Mayor=sum(comercio_mayor),C_Comercio_Menor=sum(comercio_menor),D_Manufactura=sum(manufactura),E_Primarias=sum(primarias),F_Restaurantes_Hoteles=sum(restaurantes_hoteles),G_Servicios=sum(servicios))
# Se separa la base a nivel Tipo de Empresa para poder graficar
dist_tipo_emp <- dist_tipo_emp %>% gather(key = Tipo_Empresa, value = Tot_pob_total, A_Administrativos:B_Comercio_Mayor:C_Comercio_Menor:D_Manufactura:E_Primarias:F_Restaurantes_Hoteles:G_Servicios)

#EMPRESAS VS POBLACIÓN
# Se agrupan las variables a nivel Estado
dist_total <- datos %>% select(estado,pob_total,tot_empresas) %>% group_by(estado) %>% summarise(Tot_poblacion_a=sum(pob_total),Tot_empresas_a=sum(tot_empresas))
# Calcula %
dist_total$"pctg_edo_pob"=(dist_total$Tot_poblacion_a/colSums(datos["pob_total"]))*100
dist_total$"pctg_edo_emp"=(dist_total$Tot_empresas_a/colSums(datos["tot_empresas"]))*100
dist_total$"rel_emp_pob"=(dist_total$Tot_poblacion_a/dist_total$Tot_empresas_a)
```
<br>


***
#### Análisis de la Población
***

**3.2.1 &nbsp; Distribución de la población en la República Mexicana por Sexo**
<br>
En la **gráfica 1** se puede observar que el estado de la República con más población es el Estado de México, seguido por el Distrito Federal y el estado de Jalisco. Por otro lado, la **gráfica 2** muestra que todos los estados de la República mantienen un mix cercano al 50%-50% entre totalidad de hombres y mujeres. <br>*El estado que más hombres tiene alcanza un total de ≈51% mientras que el que menos tiene alcanza un total de ≈47.3%.*

<br>

**Gráfica 1. &nbsp; Distribución de la población por Sexo y Estado**

```{r echo=FALSE}
# Se genera la gráfica con los datos anteriores 
ggplot(dist_poblacion2, aes(x=estado, y=Tot_pob_total)) + geom_bar(aes(fill=Sexo), stat="identity") + xlab("") + theme(plot.title = element_text(hjust = 0.5)) + ylab("Numero de Individuos") + theme(axis.text.x = element_text(angle = 90, hjust = 1), axis.text.y=element_blank()) 
```

**Gráfica 2. &nbsp; Porcentaje de hombres y mujeres por Estado**

```{r echo=FALSE}
# Se genera la gráfica con los datos anteriores 
ggplot(dist_poblacion2, aes(x=Sexo, y=Tot_pob_total)) + geom_boxplot() + ylab("Porcentaje de hombres y mujeres por Estado") + expand_limits(y=.465) + theme(axis.text.y=element_blank())
```


<br>

**3.2.2 &nbsp; Distribución de la población en la República Mexicana por Edad**
<br>
La **gráfica 3** muestra la distribución de la población por estado de acuerdo al rango de edad, siendo el Distrito Federal el estado de la República con mayor variación, ya que es la localidad con menor número de Niños y Jóvenes ≈38% (para el resto de los estados, este mismo segmento de población representa aproximadamente un 47% del total poblacional).

<br>

**Gráfica 3. &nbsp; Share de la población por Edad por Estado**

```{r echo=FALSE}
# Se genera la gráfica con los datos anteriores 
ggplot(dist_edad, aes(x=estado, y=Tot_pob_total)) + geom_bar(aes(fill=Edad), stat="identity", position = "fill") + xlab("") + theme(plot.title = element_text(hjust = 0.5)) + ylab("Share de Edad") + theme(axis.text.x = element_text(angle = 90, hjust = 1), axis.text.y=element_blank()) + scale_fill_manual(values=c("#008B8B", "#66CDAA","#ADD8E6","#B0C4DE","#A9A9A9"))
```


<br>

**3.2.3 &nbsp; Distribución de Hogares por Rango de Ingreso**
<br>
La **gráfica 4** permite observar la gran diferencia que existe en las condiciones de vida de la población en México; se observa una gran desigualdad económica derivada del nivel de concentración de población con ingresos marginales que existe en algunos estados. <br>En estados como Chiapas aproximadamente el 42% de los hogares perciben un ingreso entre 0 y 2 salarios mínimos mensuales, mientras que estados como Quintana Roo únicamente el ≈17% de su población se encuentra en estas condiciones.   <br> En el mapa de la **gráfica 5** se observa la concentración de la población con ingresos marginales: los estados de Chiapas, Guerrero, Yucatán y Oaxaca se encuentran sombreados de color azul obscuro pues más del 34% de su población percibe un ingreso marginal, mientras que Quintana Roo, Querétaro, Baja Californa y Baja California Sur se encuentran sombreados de color azul claro, lo cual implica que menos del 19% de su población se encuentra en este rango de ingresos.

<br>

**Gráfica 4. &nbsp; Porcentaje de hogares por tipo de Ingreso por Estado**

```{r echo=FALSE}
# Se genera la gráfica con los datos anteriores 
ggplot(dist_ingreso2, aes(x=Tipo_Ingreso, y=Pob_share)) + geom_boxplot() + ylab("Porcentaje de hogares por tipo de Ingreso por Estado") + expand_limits(y=0.15) + xlab("Tipo de Ingreso")
```
<br>

**Gráfica 5. &nbsp; Nivel de concentración de población con Ingreso Marginal**

```{r warning=FALSE, echo=FALSE}
# Variables para generar mapa
#dist_mapita <- dist_ingreso[,c("cveedo","E_Ing_Marginal")]
#names(dist_mapita) <- c("region","value")

#mxstate_choropleth(dist_mapita, 
#                   legend = "% poblacion con Ing. Marginal")
```

![](/Users/mariela/Documents/Diplomado/Bases de Datos/Poblacion Ingreso Marginal.png)

**3.2.4 &nbsp; Distribución del Nivel de Bienestar Económico por Estado**
<br>
La **gráfica 6** muestra la relación del porcentaje de la población por Nivel de Bienestar Económico por estado, esta gráfica confirma lo visto en la **gráfica 4** dónde se observa la gran desigualdad económica del país. En este caso se puede observar que el estado de Chiapas agrupa más del 70% de su población en el NSE "E" seguido por Oaxaca y Yucatán con más del 40%. La gráfica también permite observar que todos los estados tienen menos del 2% de su población en el nivel más alto (A/B+), siendo Querétaro el estado con mayor concentración de población en este nivel.

<br>

**Gráfica 6. &nbsp; Porcentaje de población por Nivel de Bienestar Económico por Estado**
```{r echo=FALSE}
# Se genera la gráfica con los datos anteriores 
ggplot(dist_bienestar, aes(x=nse_2016, y=pctg_edo, color=estado)) + geom_point() + expand_limits(y=0) +  xlab("Nivel de Bienestar Economico") + ylab("% de Poblacion") + theme(plot.title = element_text(hjust = 0.5))
```

***
#### Análisis del Sector Empresarial
***

**3.2.5 &nbsp; Distribución de las empresas en la República Mexicana por tamaño**
<br>
En la **gráfica 7** se observa que el estado de la República con mayor número de empresas es el Distrito Federal, seguido por el Estado de México, Jaliso y Puebla; los estados con menor número de empresas son Baja California, Colima, Campeche y Nayarit.   En todos los estados de la República más del 95% son Micro empresas, las cuales son un motor importante para el crecimiento de la economía nacional. <br> *El Distrito Federal es el estado que alberga el mayor número de empresas Pequeñas y Medianas.*


**Gráfica 7. &nbsp; Distribución de las empresas por Tamaño y Estado**

```{r echo=FALSE}
# Se genera la gráfica con los datos anteriores 
ggplot(dist_empresas2, aes(x=estado, y=Tot_empresas)) + geom_bar(aes(fill=Tamano_Empresa), stat="identity") + xlab("") + theme(plot.title = element_text(hjust = 0.5)) + ylab("Numero de Empresas") + theme(axis.text.x = element_text(angle = 90, hjust = 1), axis.text.y=element_blank()) 
```

**3.2.6 &nbsp; Tipo de Empresa por Estado**
<br>
Es importante para el desarrollo económico nacional la creación y desarrollo de Micro, Pequeñas, Medianas y Grandes empresas; en la **gráfica 8** se observa la participación de cada una de ellas en los mercados de los diferentes estados, es fundamental analizar esta composición y tamaño para poder entender el potencial económico de cada localidad.<br>
*El Estado de México es el estado que más empresas de "Comercio Menor"" alberga y en el que no figuran empresas como "Restaurantes y Hoteles".*


**Gráfica 8. &nbsp; Share Tipo de Empresa por Estado**

```{r echo=FALSE}
# Se genera la gráfica con los datos anteriores 
ggplot(dist_tipo_emp, aes(x=estado, y=Tot_pob_total)) + geom_bar(aes(fill=Tipo_Empresa), stat="identity", position = "fill") + xlab("") + theme(plot.title = element_text(hjust = 0.5)) + ylab("Share de Tipo Empresa") + theme(axis.text.x = element_text(angle = 90, hjust = 1), axis.text.y=element_blank()) + scale_fill_manual(values=c("#008B8B", "#66CDAA","#ADD8E6","#B0C4DE","#A9A9A9","#C0C0C0","#808080"))
```
***

#### Relación Sector Empresarial vs. Población

***

El diagrama de dispersión de la **gráfica 9** busca entender la relación entre el total de individuos en un estado vs. el total de empresas en el mismo, a simple vista parece existir una relación positiva entre ambas variables. En la **gráfica 7** se observó que el Distrito Federal y el Estado de México son los estados con mayor número de empresas (casi las mismas en ambos estados) pero debido a la alta densidad de población en el Estado de México, la relación entre empresas-población para el Estado de México es 32 a 1 (32 personas por cada empresa) mientras que la relación en el Distrito Federal es 19 a 1 (19 personas por cada empresa).

**Gráfica 9. &nbsp; Relación número de población vs número de empresas por Estado**
```{r echo=FALSE}
# Se genera la gráfica con los datos anteriores 
ggplot(dist_total, aes(x=Tot_poblacion_a, y=Tot_empresas_a, color=estado)) + geom_point() + expand_limits(y=600000) +  xlab("Numero de empresas") + ylab("Total de poblacion") + theme(legend.position="")
```

***

## Bonus Extra

***

##### 1. &nbsp;Importa Dataset

```{r}
# Asigna directorio
setwd("/Users/mariela/Documents/Diplomado/Bases de Datos")

# Importa archivo en formato Excel
datos2 <- read_excel("income_basejam.xlsx")

# Convierte en Dataframe
datos2 <- as.data.frame(datos2)
datos2["Income_Range"] <- lapply(datos2["Income_Range"], factor)
```


<br>

##### 2. &nbsp;Prepara base DENUE

Calcula el ingreso promedio por Código Postal basado en el Salario Mínimo Mensual vigente ($2,286 mensuales) y el porcentaje de población en cada rango de ingreso:
```{r}
# Base DENUE a nivel Código Postal
dist_ingreso_cp <- datos %>% select(estado,codpostal,ing_marginal,ing_bajo,ing_medio,ing_med_alto,ing_alto,pob_hombres,pob_mujeres) %>%  group_by(estado,codpostal) %>% summarise(Tot_ing_marginal=sum(ing_marginal), Tot_ing_bajo=sum(ing_bajo), Tot_ing_medio=sum(ing_medio), Tot_ing_med_alto=sum(ing_med_alto), Tot_ing_alto=sum(ing_alto)) %>% mutate(E_Ing_Marginal=Tot_ing_marginal/(Tot_ing_marginal+Tot_ing_bajo+Tot_ing_medio+Tot_ing_med_alto+Tot_ing_alto), D_Ing_Bajo=Tot_ing_bajo/(Tot_ing_marginal+Tot_ing_bajo+Tot_ing_medio+Tot_ing_med_alto+Tot_ing_alto), C_Ing_Medio=Tot_ing_medio/(Tot_ing_marginal+Tot_ing_bajo+Tot_ing_medio+Tot_ing_med_alto+Tot_ing_alto), B_Ing_Med_Alto=Tot_ing_med_alto/(Tot_ing_marginal+Tot_ing_bajo+Tot_ing_medio+Tot_ing_med_alto+Tot_ing_alto), A_Ing_Alto=Tot_ing_alto/(Tot_ing_marginal+Tot_ing_bajo+Tot_ing_medio+Tot_ing_med_alto+Tot_ing_alto))  %>% arrange(desc(E_Ing_Marginal))
# Calcula ingreso promedio por Código Postal
salario_minimo <- 2686
dist_ingreso_cp$"ingreso_promedio"=(dist_ingreso_cp$E_Ing_Marginal*salario_minimo*1)+(dist_ingreso_cp$D_Ing_Bajo*salario_minimo*3)+(dist_ingreso_cp$C_Ing_Medio*salario_minimo*7.5)+(dist_ingreso_cp$B_Ing_Med_Alto*salario_minimo*16)+(dist_ingreso_cp$A_Ing_Alto*salario_minimo*25)
# Base Final
colnames(dist_ingreso_cp)[colnames(dist_ingreso_cp)=="codpostal"] <- c("codigo_postal")
```

<br>
**Gráfica 1. &nbsp; Ingreso Promedio Estimado por CP por Estado**
```{r echo=FALSE, warning=FALSE}
# Se genera la gráfica con los datos anteriores 
ggplot(dist_ingreso_cp, aes(x=estado, y=ingreso_promedio, color=estado)) + geom_boxplot() + theme(legend.position="", axis.text.x = element_text(angle = 90, hjust = 1)) +  xlab("") + ylab("Ingreso Promedio")
```


##### 3. &nbsp;Une Bases
Se unen las bases por Código Postal de la información del cliente vs. información DENUE. &nbsp; *Es importante mencionar que no todos los cientes cruzan con la base del DENUE*
```{r}
# Se unen las bases por código postal
bases_unidas <- merge(datos2,dist_ingreso_cp,by="codigo_postal", all.x = TRUE)
```


<br>

> ¿Es posible obtener información adicional sobre el cliente?

R= Si, con ayuda de la información del DENUE podríamos saber si el Sector Laboral en el que trabaja el cliente es relevante en su localidad. Por otro lado, dado que el diccionario de la base de los clientes no indica el significado del "Rango de Ingresos", con esta base se podría deducir el orden (del 1 al 7) de esta variable para conocer cuál número indica un Ingreso más alto:

**Gráfica 2. &nbsp; ID Ingreso cliente vs. Ingreso Promedio**
```{r echo=FALSE, warning=FALSE}
# Se genera la gráfica con los datos anteriores 
ggplot(bases_unidas, aes(x=Income_Range, y=ingreso_promedio, color=Income_Range)) + geom_boxplot() + theme(legend.position="", axis.text.x = element_text(angle = 90, hjust = 1)) +  xlab("") + ylab("Ingreso Promedio del CP")
```

La variable "Income Range" ordena de manera descendente, es decir, los clientes con Ingreso = 7 tienen ingresos más altos que aquellos con valor 1.

<br>

> ¿Es posible ver si hay correspondencia entre el ingreso declarado del cliente y la caracterización socieconómica del Código Postal correspondiente??

R= Si es posible con el Ingreso Inferido por Código Postal (para los clientes donde se tiene el Código Postal).  En la **gráfica 2** todos clientes que declararon tener un "Income Range Alto" (entre 5 y 7) y que se encuentran en los puntos inferiores (valores atípicos) indica que el Ingreso Estimado para el Código Postal en el que vive el cliente no corresponde con lo declarado.