---
title: "caso_practico_1"
author: "alex_zav"
date: "6 de octubre de 2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Iportar el dataset "Denue012016_Agentessurbanos demograficos.xls"

```{r}
#importamos excel
require(readxl)
#library(readxl)
my_table <- read_excel("Denue012016_Agentessurbanos demograficos.xlsx",col_names = TRUE, skip=2)
View(my_table)

require(dplyr)
tab_inicial <- select(my_table,Estado,NSE_2016,starts_with("Hog"))
head(tab_inicial)
View(tab_inicial)

require(tidyr)
tab_ingreso <- tab_inicial %>% gather(key = rango_ingreso, value = tot_ingresos, -Estado,-NSE_2016)
View(tab_ingreso)
summary(tab_ingreso)

tab_resumen <- tab_ingreso %>%
  filter(tot_ingresos > 80) %>%
  group_by(Estado,NSE_2016,rango_ingreso) %>%
  summarize(mean_ing = mean(tot_ingresos), median_ing = median(tot_ingresos), total_ing=sum(tot_ingresos))
View(tab_resumen)  

summary(tab_resumen)

tab_ing_fin <- tab_resumen %>%
  filter(rango_ingreso == "Hog101_mas" & total_ing > 80)

library(ggplot2)
ggplot(tab_ing_fin, aes(x=Estado, y=median_ing)) + geom_col(fill="#819FF7", colour = "#070B19") + ggtitle("Distribución de hogares por salario máximo") + xlab("Estado") +   theme(axis.text.x = element_text(angle = 20, size = 6,hjust = 1, vjust = 1))  + ylab("Total de hogares con ingresos mayores ")
  

ggplot(tab_resumen, aes(x=rango_ingreso, y=median_ing, color=Estado )) + geom_point() + theme(axis.text.x = element_text(angle = 20, size = 6,hjust = 1, vjust = 1))

# + facet_wrap(~year)

```

#Revisamos los primeros registros
```{r}
require(dplyr)
head(select(my_table,Anio_info, Estado, Municipio, NSE_2016, Pob_total, Hog_tot, Tot_empresas))
```

#Analizando las variables a utilizar
```{r}
sub_set <- select(my_table,Anio_info, Estado, Municipio, NSE_2016, Pob_total, Hog_tot, Tot_empresas)

names(sub_set) <- tolower(names(sub_set))

class(sub_set)
typeof(sub_set)
str(sub_set)
sapply(sub_set, typeof)
View(sub_set)



#require(handle)
#describe(sub_set)
```


```{r}

require(readxl)
#library(readxl)
info_adi <- read_excel("income_basejam.xlsx",col_names = TRUE)
head(info_adi)
View(info_adi)

require(dplyr)
piv <- select(my_table,Estado, NSE_2016, Pob_total, Hog_tot,Hog0_2
,Hog2_4
,Hog4_7
,Hog7_11
,Hog11_16
,Hog16_21
,Hog21_41
,Hog21ymas
,Hog41_61
,Hog61_101
,Hog101_mas, CodPostal)
head(piv)
head(info_adi)

piv_com <- merge(piv, info_adi,  by.x = "CodPostal", by.y = "codigo_postal")
head(piv_com)
View(piv_com)


